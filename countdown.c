///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//       With the Reference date of January 21 04:26:07 2014, the program will format this date like so:
//       Reference time: Wed Jan 21 04:26:07 2014
//       And will then start counting twords that value in the following format:
//       Years: 8  Days: 25  Hours: 12  Minutes: 55  Seconds: 3    
//
// @author Zack Lown <zlown@hawaii.edu>
// @date   3/15/2022
///////////////////////////////////////////////////////////////////////////////
#include    <stdlib.h>
#include    <time.h>
#include    <stdio.h>
#include    <unistd.h>//For sleep function
#include    <stdbool.h>// For boolean values
#include    <string.h>


//Reference Date, Time & Timezone
const int rYear = 2014, rMonth = 0, rDay = 21, rHour = 04, rMinute = 26, rSecond = 07;

int main() {
    time_t refTime;//Used in the struct
    time_t currTime;// Current time in seconds stored as long int.Will be set to time(0) in forever loop
    time_t timeTill;//Initialize var for holding difference of current time to reference time
    struct tm *rTimeInfo; //Struct for holding reference time as defined in the const above. For printing reference
    struct tm *differenceTimeInfo;// Used to hold fields for time_t when
    struct tm buf;
    //char printCountdown[100];
    
    
    //Take constant values and input it into the struct
    rTimeInfo=localtime(&refTime);
    rTimeInfo->tm_year=rYear-1900;
    rTimeInfo->tm_mon=rMonth;
    rTimeInfo->tm_mday=rDay;
    rTimeInfo->tm_hour=rHour;
    rTimeInfo->tm_min=rMinute;
    rTimeInfo->tm_sec=rSecond;
    
    //Print the Reference time with ascTime which formats the struct niceley
    printf("Reference time: %s", asctime(rTimeInfo));
    while( 1 ){//Forever...
        currTime = time(0);//Set currTime to system time. Important that this is in loop
        timeTill = labs(mktime(rTimeInfo) - currTime);//Calculate seconds apart. Used labs() to avoid truncate error
        differenceTimeInfo = localtime_r(&timeTill, &buf);//Converts seconds to calendar time
        printf("Years: %d  Days: %d  Hours: %d  Minutes: %d  Seconds: %d \n", differenceTimeInfo->tm_year-70, differenceTimeInfo->tm_mday, differenceTimeInfo->tm_hour, differenceTimeInfo->tm_min, differenceTimeInfo->tm_sec);
        sleep(1);//Wait one second
    }
}//End of main  
